# Defined in - @ line 1
function n --wraps='nitrogen --random --set-zoom-fill' --description 'alias n=nitrogen --random --set-zoom-fill'
  nitrogen --random --set-zoom-fill $argv;
end
